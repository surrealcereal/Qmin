# Qmin

#### A small script to retrieve minimized windows in Qtile using rofi.

Qmin, pronounced _\ˈkyü-mən\\_, as in one of the valid pronounciations of cumin, is a reimplementation of emanuelep57's [Qminimize](https://github.com/emanuelep57/Qminimize).

## Features
- Ability to disable window IDs in rofi prompt,
- Ability to autofocus on unminimize,
- Less overhead through integration into Qtile rather than using an external command client,
- EWMH dependency removed in favor of native Qtile,
- Extended error handling and user feedback:  
  ‣ `.desktop` files are no longer assumed to contain icons.  
  ‣ It is no longer assumed that minimized windows exist.  
  ‣ It is no longer assumed that a selection in rofi is made.

## Goals
- To be more Pythonic and readable through implementing:  
  ‣ more modern language features, such as fstrings;  
  ‣ and more appropriate library calls.

## Dependencies
- [rofi](https://github.com/davatorium/rofi), to construct the front facing part of the script
- [fuzzywuzzy](https://pypi.org/project/fuzzywuzzy/), to allow for fuzzy matching of icons

## Installation

```bash
cd ~/.config/qtile
git clone https://codeberg.org/surrealcereal/Qmin.git
mv Qmin/Qmin* .
rm -rf Qmin
```

## Usage
As this script makes use of internals, it can directly be assigned to a key inside `config.py` after being imported.
```python
from Qmin import qmin

Key([mod, "shift"], "m", qmin(), desc="Qmin unminimizer")
```
Unlike Qminimize, this script does not provide flags and the ability to minimize. This can be achieved using the builtin commands:
```python
Key([mod], "m", lazy.window.toggle_minimize(), lazy.group.next_window(), desc="Minimize window")
```

## Configuration

All configuration is handled within `Qminconfig.py`, which is provided alongside the main script file.

Documentation for the various configuration options are also provided inside this file, please consult said documentation to see what the options are for and how to use them.

